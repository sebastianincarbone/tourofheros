import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ParaPyME';
  mensaje = true;
  toggleM() {
    this.mensaje = !this.mensaje;
  }
}
